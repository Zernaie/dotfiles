# Personal qtile config 

import os
import re
import socket
import subprocess
from libqtile import qtile
from typing import List  # noqa: F401
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy

mod = "mod4"
terminal = "alacritty" 

mod = "mod4"
keys = [
    Key([mod], "j", lazy.layout.down(),
        desc='Move focus down in current stack pane'),
    Key([mod], "k", lazy.layout.up(),
        desc='Move focus up in current stack pane'),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), lazy.layout.section_down(),
        desc='Move windows down in current stack'),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), lazy.layout.section_up(),
        desc='Move windows up in current stack'),
    Key([mod], "h", lazy.layout.shrink(), lazy.layout.decrease_ratio(),
        desc='Shrink window (MonadTall), decrease ratio (Tile)'),
    Key([mod], "l", lazy.layout.grow(), lazy.layout.increase_ratio(),
        desc='Expand window (MonadTall), increase ratio (Tile)'),
    Key([mod], "n", lazy.layout.normalize(),
        desc='normalize window size ratios'),
    Key([mod], "m", lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), 
        desc="Launch terminal"),
    Key([mod], "space", lazy.window.toggle_floating(),
        desc='toggle floating'),
    Key([mod], "f", lazy.window.toggle_fullscreen(),
        desc='toggle fullscreen'),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), 
        desc="Toggle between layouts"),
    Key([mod, "shift"], "q", lazy.window.kill(), 
        desc="Kill focused window"),
    Key([mod, "shift"], "r", lazy.restart(), 
        desc="Restart Qtile"),
    Key([mod, "shift"], "e", lazy.shutdown(), 
        desc="Shutdown Qtile"),
    Key([mod], "d", lazy.spawn('j4-dmenu-desktop'),
        desc="Spawn a command using a prompt widget"),
    Key(["control", "mod1"], "f", lazy.spawn('sharenix-section'),
        desc="Personal screenshot tool"),

    # Laptop keys
    Key([], "XF86AudioMute", lazy.spawn("  amixer set Master 1+ toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume 0 -5%")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume 0 +5%")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 5")),
    
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=False),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

# COLORS 
colors = [
    ["#ffffff",],  # 0 Foreground (White)
    ["#644CA0",],  # 1 Accent (Red) 
    ["#212329",],  # 2 Background (Black/Dark Grey)
    ["#4d4d4d",],  # 3 Bar Inactive (Light Grey)
]

layout_theme = {
    "ratio_increment": 0.05,
    "border_width": 2,
    "margin": 8,
    "border_focus":  colors[1],
    "border_normal": colors[3], 
}

widget_defaults = dict(
    font='Ubuntu',
    fontsize=12,
    padding=3,
    background=colors[2],
)

layouts = [
    # Try more layouts by unleashing below layouts.
    layout.Tile(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    # layout.Columns(),
    # layout.Stack(),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Sep(
                        background = colors[1],   
                        foreground = colors[1],
                        padding = 6),
                widget.TextBox(
                        text = '',
                        font = "Ubuntu",
                        background = colors[1],
                        foreground = colors[0],
                        padding = 4,
                        fontsize = 38),
                widget.TextBox(
                        text = '',
                        font = "Ubuntu",
                        background = colors[2],
                        foreground = colors[1],
                        padding = 0,
                        fontsize = 40),
                widget.GroupBox(
                        font = "Ubuntu bold",
                        margin_y = 3,
                        fontsize = 12,
                        borderwidth = 4,
                        active = colors[0],
                        inactive = colors[3],
                        rounded = True,
                        highlight_method = "block",
                        block_highlight_text_color = colors[0],
                        this_current_screen_border = colors[1],
                        foreground = colors[0],
                        background = colors[2],
                        disable_drag = True),
                widget.Sep(
                        background = colors[2],   
                        foreground = colors[2],
                        padding = 4),
                widget.CurrentLayout(
                        font = "Ubuntu bold",
                        fontsize = 12,
                        background = colors[2],
                        foreground = colors[0]),
                widget.TextBox(
                        text = '',
                        font = "Ubuntu",
                        background = colors[1],
                        foreground = colors[2],
                        padding = 0,
                        fontsize = 40),
                widget.WindowName(
                        font = "Ubuntu bold",
                        fontsize = 12,  
                        background = colors[1],
                        foreground = colors[0],
                        padding = 0,
                        max_chars = 60),
                widget.CapsNumLockIndicator(
                        font = "Ubuntu bold",
                        fontsize = 12,  
                        background = colors[1],
                        foreground = colors[0]),
                widget.TextBox(
                        text = '',
                        font = "Ubuntu",
                        background = colors[1],
                        foreground = colors[2],
                        padding = 0,
                        fontsize = 40),
                widget.Systray(
                        font = "Ubuntu bold",
                        foreground = colors[0],
                        padding = 5,
                        fontsize = 12),
                widget.Sep(
                        foreground = colors[2],
                        padding = 15),
                widget.TextBox(
                        text = '',
                        font = "Ubuntu",
                        foreground = colors[1],
                        padding = 0,
                        fontsize = 30),
                widget.CPU(
                        font = "Ubuntu bold",
                        foreground = colors[0],
                        padding = 5,
                        fontsize = 12,
                        format = ' {load_percent}%'),
                widget.Sep(
                        foreground = colors[2],
                        padding = 8),
                widget.TextBox(
                        text = '',
                        font = "Ubuntu",
                        foreground = colors[1],
                        padding = 0,
                        fontsize = 30),
                widget.Memory(
                        font = "Ubuntu bold",
                        foreground = colors[0],
                        padding = 5,
                        fontsize = 12,
                        measure_mem = 'G',
                        format = '{MemUsed: .1f}{mm}/{MemTotal: .1f}{mm}'),
                widget.Sep(
                        foreground = colors[2],
                        padding = 8),
                widget.TextBox(
                        text = '',
                        font = "Ubuntu",
                        foreground = colors[1],
                        padding = 0,
                        fontsize = 30),
                widget.Volume(
                        font = "Ubuntu bold",
                        fontsize = 12,
                        foreground = colors[0],
                        fmt = ' {}',
                        padding = 5),
                widget.Sep(
                        foreground = colors[2],
                        padding = 8),
                widget.TextBox(
                        text = '',
                        font = "Ubuntu",
                        foreground = colors[1],
                        padding = 0,
                        fontsize = 30),
                widget.Clock(
                        format='%A,  %B %d, %l:%M %p',
                        font = "Ubuntu bold",
                        fontsize = 12,
                        padding=5,
                        foreground = colors[0]),
                widget.Sep(
                        foreground = colors[2],
                        padding = 5),
                ],
            28,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(**layout_theme, float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry

])

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
