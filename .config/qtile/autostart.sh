#!/usr/bin/env bash

killall -q autostart.sh

pgrep -x picom > /dev/null || picom &
pgrep -x nm-applet > /dev/null || nm-applet &
pgrep -x xfce4-clipman > /dev/null || xfce4-clipman &
pgrep -x xfce4-power-manager > /dev/null || xfce4-power-manager &
pgrep -x lxpolkit > /dev/null || lxpolkit & 

xsetroot -cursor_name left_ptr &
nitrogen --restore

